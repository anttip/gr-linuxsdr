/* -*- c++ -*- */

#define LINUXSDR_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "linuxsdr_swig_doc.i"

%{
#include "linuxsdr/libv4l2_x.h"
%}


%include "linuxsdr/libv4l2_x.h"
GR_SWIG_BLOCK_MAGIC2(linuxsdr, libv4l2_x);

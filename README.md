gr-linuxsdr
===========

Video4Linux GNU Radio plugin


install:
$ git clone git://linuxtv.org/anttip/gr-linuxsdr.git
$ cd gr-linuxsdr/
$ mkdir build
$ cd build/
$ cmake ../
$ make
$ sudo make install

run:
$ export GRC_BLOCKS_PATH=/usr/local/share/gnuradio/grc/blocks
$ export PYTHONPATH=/usr/local/lib64/python2.7/site-packages
$ export LD_LIBRARY_PATH=/usr/local/lib64
$ gnuradio-companion
